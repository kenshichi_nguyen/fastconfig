<?php
include_once 'views/head.phtml';
/**
 * Created by PhpStorm.
 * User: Ken
 * Date: 10/28/2014
 * Time: 4:46 PM
 */
$host = 'localhost';
$user = 'root';
$pass = 'root';
$dbname = 'test';
require_once 'views/dbfrm.phtml';
$demoMail = 'demo@superbbg.com';
if (!empty($_POST)){
    if($_POST['host'] != '')  $host = $_POST['host'];
    if($_POST['user'] != '')  $user = $_POST['user'];
    if($_POST['pass'] != '')  $pass = $_POST['pass'];
    if($_POST['dbname']!='')  $dbname = $_POST['dbname'];

    try {
        $db = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
        echo "<p class='alert-danger'>Database: <strong>$dbname</strong></p>";
    } catch (PDOException $e) {
        echo "<p class='alert-danger'>Error!: " . $e->getMessage() . "</p>";
        die();
    }
}

if(isset($_POST['changeEmail'])){
    if($_POST['newEmail'] != '') $demoMail = $_POST['newEmail'];
    $sql = "update core_config_data set value = '$demoMail' WHERE path like '%email%' and value like '%@%'";
    $count = $db->exec($sql);
    echo "<p class='alert-success'>Update $count rows to $demoMail.</p>";
    if($count>0){
        $select = "SELECT * FROM core_config_data WHERE path like '%email%' and value like '%@%'";
        showResult($db,$select);
    }

}

if(isset($_POST['changeUrl'])){
    if($_POST['newUrl'] != '') $demoUrl = $_POST['newUrl'];
    $sql = "update core_config_data set value = '$demoUrl' WHERE path like 'web%base_url%'";
    $count = $db->exec($sql);
    echo "<p class='alert-success'>Update $count URLs to $demoUrl.</p>";
    if($count>0) {
        $select = "SELECT * FROM core_config_data WHERE path like 'web%base_url%'";
        showResult($db, $select);
    }
}

if(isset($_POST['mailDisable'])){
    $sql = "update core_config_data set value = '0' WHERE path like '%email%enabled%'";
    $count = $db->exec($sql);
    echo "<p class='alert-success'>Update $count emails to disable.</p>";
    if($count>0) {
        $select = "SELECT * FROM core_config_data WHERE path like '%email%enabled%'";
        showResult($db, $select);
    }
}
if(isset($_POST['mailEnable'])){
    $sql = "update core_config_data set value = '1' WHERE path like '%email%enabled%'";
    $count = $db->exec($sql);
    echo "<p class='alert-success'>Update $count emails to enable.</p>";
    if($count>0) {
        $select = "SELECT * FROM core_config_data WHERE path like '%email%enabled%'";
        showResult($db, $select);
    }
}

function showResult($db,$sql)
{
    $result = $db->query($sql);
    $i=1;
    foreach($result as $value) {
        echo "<p class='alert-info'>".$i.". ".$value['path'] . "   :    <span style='float:right'>" . $value['value'] . "</span></p>";
        $i++;
    }
}
include_once 'views/footer.phtml';
